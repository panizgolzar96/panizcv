package com.cv.PanizGolzarCV.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class HomeController
{

    @GetMapping("")
    public String home(){
        return "Hello world";
    }

    @GetMapping("hi")
    public String sayHelloBack(){
        return "Hi back endpoint get";
    }

    @PostMapping("contact-me")
    public String ContactMeForm(@RequestBody FormModel name){
        return "ty ke ba man contact kardid";
    }

    @PostMapping("hi/{name}")
    public String addAndReturnType2(@PathVariable String name){
        return "Salam via PathVariable " + name;
    }

}