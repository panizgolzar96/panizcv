package com.cv.PanizGolzarCV;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanizGolzarCvApplication {
	public static void main(String[] args) {
		SpringApplication.run(PanizGolzarCvApplication.class, args);
	}

}
